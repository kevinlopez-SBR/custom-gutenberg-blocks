<?php
/*
Plugin Name: Custom Gutenberg
Description:        Custom gutenberg blocks.
Author:             Kevin L U
Version:            0.0.1
*/
function register_static_blocks() {

    // automatically load dependencies and version
    $asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

    wp_register_script(
        'custom-gutenberg-js',
        plugins_url( 'build/index.js', __FILE__ ),
        $asset_file['dependencies'],
        $asset_file['version']
    );

    register_block_type( 'custom-gutenberg/first-block', array(
        'editor_script' => 'custom-gutenberg-js',
    ));
}
add_action( 'init', 'register_static_blocks' );

function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'my-category',
                'title' => __( 'My category', 'my-plugin' ),
                'icon'  => 'wordpress',
            ),
            array(
                'slug' => 'static-blocks',
                'title' => __( 'Static Blocks', 'static-blocks' ),
                'icon'  => 'wordpress',
            ),
            array(
                'slug' => 'dynamic-blocks',
                'title' => __( 'Dynamic Blocks', 'dynamic-blocks' ),
                'icon'  => 'wordpress',
            ),
        )
    );
}

add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


//Dynamic blocks

function register_dynamic_blocks() {
    register_block_type( 'custom-gutenberg/second-block', array(
        'editor_script' => 'custom-gutenberg-js',
        'render_callback' => 'render_second_block'
    ));
}

add_action( 'init', 'register_dynamic_blocks' );

function render_second_block($block_attributes, $content) {
    return include( plugin_dir_path( __FILE__ ) . 'src/renders/second_block.php');
}
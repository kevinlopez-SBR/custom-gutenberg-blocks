const defaultConfig = require("@wordpress/scripts/config/webpack.config")

module.exports = {
  ...defaultConfig,
  resolve: {
    ...defaultConfig.resolve,
    modules: ['node_modules', 'src'],
  },
}

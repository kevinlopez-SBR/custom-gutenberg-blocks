import { TextControl } from '@wordpress/components';

const { createElement } = wp.element

const Preview = ({ attributes }) => {
  const {
    title,
  } = attributes

  return (
    <div className="second-block-wrapper">
      <span>{title}</span>
    </div>
  )
}

const Form = ({ setAttributes, attributes }) => {
  const {
    title,
  } = attributes

  return (
    <div className="second-block-wrapper form">
      <TextControl
        label="Type a title"
        value={title}
        onChange={(title) => setAttributes({ title })}
      />
    </div>
  )
}

export default (props) => {
  const { isSelected } = props

  return (createElement(isSelected ? Form : Preview, props))
}

import { __ } from '@wordpress/i18n';

import edit from './edit'

export const name = 'custom-gutenberg/second-block'

export const settings = {
  title: __('Second Block'),
  description: __('description'),
  keywords: [__( 'second'), __('block')],
  icon: 'table-row-delete',
  attributes: {
    title: { type: 'string' }
  },
  edit,
}
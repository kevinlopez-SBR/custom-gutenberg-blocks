const { createElement } = wp.element

const Preview = ({ attributes }) => {
  const {
    title,
  } = attributes

  return (
    <div className="first-block-wrapper">
      This is the preview
    </div>
  )
}

const Form = ({ attributes }) => {
  const {
    title,
  } = attributes

  return (
    <div className="first-block-wrapper form">
      This is the form
    </div>
  )
}

export default (props) => {
  const { isSelected } = props

  return (createElement(isSelected ? Form : Preview, props))
}

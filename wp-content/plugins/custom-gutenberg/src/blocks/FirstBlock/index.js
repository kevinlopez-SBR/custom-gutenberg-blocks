import { __ } from '@wordpress/i18n';

import edit from './edit'
import save from './save'

export const name = 'custom-gutenberg/first-block'

export const settings = {
  title: __('First Block'),
  description: __('description'),
  keywords: [__( 'first'), __('block')],
  icon: 'table-row-delete',
  attributes: {
    title: { type: 'string' }
  },
  edit,
  save,
}
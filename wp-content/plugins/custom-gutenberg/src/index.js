import { registerBlockType } from '@wordpress/blocks';

import * as FirstBlock from './blocks/FirstBlock';
import * as SecondBlock from './blocks/SecondBlock';

const register = ({ name, settings }, category) => registerBlockType(name, { ...settings, category });

register(FirstBlock, 'static-blocks')
register(SecondBlock, 'dynamic-blocks')
